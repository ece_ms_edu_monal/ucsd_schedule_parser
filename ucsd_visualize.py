import re
from datetime import datetime
from os.path import join as ospjoin
from os.path import expanduser
import json
import time as tm
import datetime, dateutil.parser
import pytz, numpy as np
import natsort
import seaborn as sns, matplotlib.pyplot as plt
import argparse
from matplotlib.backends.backend_pdf import PdfPages
sns.set(color_codes=True)
home = expanduser(".")


############################################
# Generate histograms of room utilization and save as pdf
############################################


#args: year, quarter, input_name, and output_location
parser = argparse.ArgumentParser()
parser.add_argument('--room_of_interest',required=True)
parser.add_argument('--year', dest='year', required=True)
parser.add_argument('--quarter', dest='quarter', required=True)
parser.add_argument('--input_json', default=ospjoin(home,parser.parse_args().year+"_"+parser.parse_args().quarter+"_UCSD_classes.json"))
parser.add_argument('--output_location', default=home)

args = parser.parse_args()


quarter=args.quarter
year=args.year
input_name=args.input_json
out_folder=args.output_location
room_of_interest=args.room_of_interest

#load json
with open(input_name) as data_file:
    ucsd = json.load(data_file)


#Json parsing definitions
timezone_pst = pytz.timezone('US/Pacific')
days_full = {"0":"Monday", "1":"Tuesday","2":"Wednesday", "3":"Thursday", "4":"Friday","5":"Saturday","6":"Sunday"}
days_abb = {"M":"0", "Tu":"1", "W":"2", "Th":"3", "F":"4", "S":"5", "Sun":"6"}
day_arr=[0,1,2,3,4,5,6]


#Always wrap the creation of a datetime.datetime() instance with this
def localtime_to_utc(naive_datetime):
    return pytz.utc.normalize(timezone_pst.localize(naive_datetime))

def parse_utc_string_datetime(dstring):
    assert isinstance(dstring,str), str(type(dstring))
    return pytz.utc.normalize(dateutil.parser.parse(dstring))

def multipage(filename, figs=None, dpi=200):
    pp = PdfPages(filename)
    if figs is None:
        figs = [plt.figure(n) for n in plt.get_fignums()]
    for fig in figs:
        fig.savefig(pp, format='pdf')
    pp.close()



#template for weekly utilization 0 is Monday, 6 is Sunday; used to make utilization histograms
utilization={"room":{}, "duration":0,"0": {"duration":0,"frequency":{"6-8":0,"8-10":0,"10-12":0,"12-14":0,"14-16":0,"16-18":0,"18-20":0,"20-23":0}}, "1": {"duration":0,"frequency":{"6-8":0,"8-10":0,"10-12":0,"12-14":0,"14-16":0,"16-18":0,"18-20":0,"20-23":0}}, "2": {"duration":0,"frequency":{"6-8":0,"8-10":0,"10-12":0,"12-14":0,"14-16":0,"16-18":0,"18-20":0,"20-23":0}}, "3": {"duration":0,"frequency":{"6-8":0,"8-10":0,"10-12":0,"12-14":0,"14-16":0,"16-18":0,"18-20":0,"20-23":0}}, "4": {"duration":0,"frequency":{"6-8":0,"8-10":0,"10-12":0,"12-14":0,"14-16":0,"16-18":0,"18-20":0,"20-23":0}}, "5": {"duration":0,"frequency":{"6-8":0,"8-10":0,"10-12":0,"12-14":0,"14-16":0,"16-18":0,"18-20":0,"20-23":0}}, "6": {"duration":0,"frequency":{"6-8":0,"8-10":0,"10-12":0,"12-14":0,"14-16":0,"16-18":0,"18-20":0,"20-23":0}}}



ses_freq={}
most={}
total_class_num=0
time_gap=2
rm_json={}
room_arry=[]
sort_arry=[]

#Iterate through departments
for dpt, data in ucsd["data"].items():
    
    #Iterate through classes in department
    for c_name, c_data in data["classes"].items():

        if "sessions" in c_data:
            di_check=0
            for ses_name, session_data in c_data["sessions"].items():

                sn = ses_name.split('_')[0]

                #ignore finals
                if ses_name != "fi": #and sn=='le' or sn=='se' or sn=='pb' or sn=='re' or sn=='di' :

                    if sn in ses_freq:
                        ses_freq[sn]+=1
                    else:
                        ses_freq[sn]=1


                    if "location" in session_data:
                        room=session_data["location"]

                        
                        #Check specific room schedules
                        if args.room_of_interest:
                            if room.lower() == room_of_interest.lower():
                                 rm_of_interest= c_name+'_'+sn, ' in ', room, c_data['course_name'], session_data["days"], session_data['time'], ' prof: ',session_data['professor']
                                 sort_interest=session_data["days"], session_data['time'], ' prof: ',session_data['professor']
                                 sort_arry.append(' '.join(map(str,sort_interest)))
                                 room_arry.append(' '.join(map(str,rm_of_interest)))
                     
                       

                        if sn=='le' or sn=='se':
                            total_class_num += 1
                            if room in most:
                                most[room].append(c_name+'_'+sn)
                            else:
                                most[room]=[c_name+'_'+sn]

                        if ses_name =='di':
                            di_check=1

                        if room not in utilization["room"]:
                            rm_json[room]=[]
                            utilization["room"][room] = {"0": 0, "1": 0, "2": 0, "3": 0, "4": 0, "5": 0, "6": 0, "frequency": 0,
                                                 'num_class': 0, "duration": 0}
                        rm_json[room].append(c_name+'_'+sn)


                        time = session_data['time']
                        start = time['start']['hour']+time['start']['minute']*1.0/60
                        end =time['end']['hour']+time['end']['minute']*1.0/60
                        dur = end - start
                        increment=1

                        if  ses_name.split('_')[0] == 're' or ses_name.split('_')[0] == 'pb' and '/' in session_data['section']:
                            dur=dur/10
                            increment=increment/10.0


                        for day in session_data["days"]:
                            utilization[days_abb[day]]["duration"]+= dur
                            t1=time['start']['hour']+int(round(time['start']['minute']/60))
                            t2=time['end']['hour']+int(round(time['end']['minute']/60))


                            if t1 <8:
                                utilization[days_abb[day]]["frequency"]['6-8'] += increment
                                if t2-t1>2:
                                    t1+=time_gap
                            if t1 >= 8 and t1 <10:
                                utilization[days_abb[day]]["frequency"]['8-10'] += increment
                                if t2-t1>2:
                                    t1+=time_gap
                            if t1 >= 10 and t1 < 12:
                                utilization[days_abb[day]]["frequency"]['10-12'] += increment
                                if t2-t1>2:
                                    t1+=time_gap
                            if t1 >= 12 and t1 < 14:
                                utilization[days_abb[day]]["frequency"]['12-14'] += increment
                                if t2-t1>2:
                                    t1+=time_gap
                            if t1 >= 14 and t1 < 16:
                                utilization[days_abb[day]]["frequency"]['14-16'] += increment
                                if t2-t1>2:
                                    t1+=time_gap
                            if t1 >= 16 and t1 < 18:
                                utilization[days_abb[day]]["frequency"]['16-18'] += increment
                                if t2-t1>2:
                                    t1+=time_gap
                            if t1 >= 18 and t1 < 20:
                                utilization[days_abb[day]]["frequency"]['18-20'] += increment
                                if t2-t1>2:
                                    t1+=time_gap
                            if t1 >= 20 and t1 < 23:
                                utilization[days_abb[day]]["frequency"]['20-23'] += increment
                                if t2-t1>2:
                                    t1+=time_gap

                            utilization["room"][room]["duration"] += dur
                            utilization["room"][room][days_abb[day]] += dur
                            utilization["duration"]+=dur
                            utilization["room"][room]["frequency"] += 1




#print data about room of interest
if args.room_of_interest:
    sort_index_rm_of_interest=natsort.index_natsorted(sort_arry)
    room_arry=[room_arry[i] for i in sort_index_rm_of_interest]
    for interest in room_arry:
        print(interest)


print('\nSession Type Frequency: ', ses_freq)

daily_data=np.array([[0,1,2,3,4,5,6],[utilization["0"]["duration"],utilization["1"]["duration"],utilization["2"]["duration"],
                                       utilization["3"]["duration"],utilization["4"]["duration"],utilization["5"]["duration"],
                                       utilization["6"]["duration"]]])

#Make utilization graphs
plt.bar(daily_data[0],daily_data[1])
plt.xlabel('Day of Week: (0:Monday-6:Sunday)')
plt.ylabel('Hours of Room Utilization ('+str(int(utilization["duration"]))+' total)')
plt.title(quarter + ' ' + year +  ' -Daily Campus Hours of Room Utilization vs Day of the Week')
plt.yticks(np.arange(0, 2400, 200))
pt_iter= daily_data.T.astype(int)
for pt in pt_iter:
    plt.annotate(pt[1], xy=(pt[0]-.25,pt[1]+50), textcoords='data')
for i in day_arr:
    plt.figure(i+2)
    my_xticks = ["6-8","8-10","10-12","12-14","14-16","16-18","18-20","20-23"]
    plt.xticks([0,1,2,3,4,5,6,7], my_xticks)

    plt.bar([0,1,2,3,4,5,6,7],[utilization[str(i)]["frequency"]["6-8"], utilization[str(i)]["frequency"]["8-10"],
    utilization[str(i)]["frequency"]["10-12"], utilization[str(i)]["frequency"]["12-14"],
    utilization[str(i)]["frequency"]["14-16"], utilization[str(i)]["frequency"]["16-18"],
    utilization[str(i)]["frequency"]["18-20"], utilization[str(i)]["frequency"]["20-23"]])
    plt.ylabel('Number of Sessions - (lecture, discussion, etc)')
    plt.yticks(np.arange(0, 500, 25))
    plt.xlabel('Time of Day')
    plt.title(quarter+" - "+days_full[str(i)]+' Session Times')


#preprocessing for room duration and frequency graphs
#for thresh in range(0,80,5):
room_rec_dur = []
room_rec_freq = []
num_rm = 0
for rm,rd in utilization["room"].items():
    if rm !='tba_TBA':
        if int(rd["frequency"])>0:
            room_rec_dur.append(int(rd["duration"]))
            room_rec_freq.append(int(rd["frequency"]))
            num_rm+=1

with open(ospjoin('./num_classes_'+str(year)+'_'+quarter+'_room_freq.json'), 'w') as outfile:
    json.dump(rm_json, outfile)


'''
print('median number of classes per room: ')
print('number of classes in room:')
for k in sorted(most, key=lambda k: len(most[k]), reverse=True):
        print( k, len(most[k]))
'''

#generate pdf report of room utilizations
plt.figure(10)
plt.hist(room_rec_dur,range(1,62,5))
plt.title(quarter + ' ' + year +  ' - # of rooms vs hours utilized per week ('+str(num_rm)+' rooms total)')
plt.ylabel('Number of Rooms')
plt.xlabel(' hours utilized per week')
plt.yticks(np.arange(0, 170, 10))
plt.xticks(np.arange(1, 60, 5))
plt.figure(11)
plt.hist(room_rec_freq,range(1,62,5))
plt.title(quarter + ' ' + year +  ' -  # of rooms vs # of sessions per week ('+str(num_rm)+' rooms total)')
plt.ylabel('Number of Rooms')
plt.xlabel(' sessions per week')
plt.yticks(np.arange(0, 170, 10))
plt.xticks(np.arange(1, 60, 5))
multipage('./UCSD_Classroom_Utilization_'+quarter+'_'+year+'.pdf')

