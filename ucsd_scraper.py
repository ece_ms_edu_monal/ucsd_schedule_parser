from bs4 import BeautifulSoup
import re
from datetime import datetime
from os.path import join as ospjoin
from os.path import expanduser
import json
import os
import time as tm
import argparse
current = expanduser(".")


############################################
# Takes a UCSD term htm file downloaded from ucsd's
# course schedule and parses it to create a json
############################################


#args: year, quarter, input_name, and output_location
parser = argparse.ArgumentParser()
parser.add_argument('--year', dest='year', required=True)
parser.add_argument('--quarter', dest='quarter', required=True)
parser.add_argument('--htm_input_file')
parser.add_argument('--output_location', default=current)
args = parser.parse_args()


quarter=args.quarter
year=args.year
out_folder=args.output_location
if args.htm_input_file:
    htm_input_file=args.htm_input_file
else:
    htm_input_file="./"+quarter+"_"+year+"_Student_Class_Info.htm"
    if not os.path.isfile(htm_input_file):
        print('Exiting, add input file arg or place htm file in repo root in format: ', htm_input_file)
    exit()


print("\nWill take a few minutes, data load time and parsing is slow")

#load in the data
start_t=tm.time()
soup = BeautifulSoup(open(htm_input_file),"html.parser")
print("\nData load time: "+str(round(tm.time()-start_t,1))+" seconds")


#scraper is hardcoded to work with web schedule structure from UCSD
days_abb = ["M", "Tu", "W", "Th", "F", "S", "Sun"]
total_classes = 0
d_list = {}
UCSD = {}
department={}
room_freq={}
start_t=tm.time()


#these are the department selected in the search
full= {'majors_searched': {}}
for a in soup.find("div",{"class":"msg confirm"}).find_all('li'):
    if '-' in str(a.string):
             major=str(a.string).split("-")
             full['majors_searched'][major[0].strip()]=major[1].strip()


#delimiter to separate classes by the department
spt="<span class=\"centeralign\">"


#get classes in each department
dpt_classes= soup.prettify().split(spt)[::2][1:]


#get department info, only contains department name and abbreviation
dpt= soup.prettify().split(spt)[1::2]


def parse(times):
    split_times = times.split('-')
    parsed_times = []
    parsed_group = []
    for time_str in split_times:
        parsed_time = datetime.strptime(time_str.strip() + 'm', '%I:%M%p')
        parsed_group.append(parsed_time.hour)
        parsed_group.append(parsed_time.minute)
    return parsed_group


#Iterate through departments
for i in range(len(dpt_classes)):

    all_in_dpt = dpt_classes[i].split("<td class=\"crsheader\">")
    count = 0
    all_in_dpt = all_in_dpt[2:]
    all_in_dpt = all_in_dpt[::2]

    full, short = str(dpt[i]).split(")")[0].strip().split("(")
    short = short.lower()
    full = full.lower()
    #print full
    d_list[short] = full.strip()

    department[short] = {"full_name": full.strip()}
    department[short]["classes"] = {}
    num_classes = 0

    # iterate through classes in department
    for c in all_in_dpt:
        cls = BeautifulSoup("<td> " + c, "html.parser")

        # separate class table into elements
        c_data = cls.find_all("td", {"class": "brdr"})
        c_data = c_data[1:]
        cdl = len(c_data)

        # find index of sessions
        ses_idx = []
        sec_list = {}
        for ii in range(cdl):
            c_detail = c_data[ii]
            spec = BeautifulSoup(str(c_detail), "html.parser")
            if spec.find("span", {"id": "insTyp"}) is not None:
                ses_idx.append(ii)
                section = str(BeautifulSoup(str(c_data[ii + 1]), "html.parser").string).strip().lower()
                typ = str(spec.find("span", {"id": "insTyp"}).string).lower().strip()
                sec_list[typ]=section

        # course number
        number = short+"_"+str(cls.find("td").string).strip().lower()

        if 'fi' in sec_list:
            sec_list.pop('fi')
        # if section append to class number
        if 'le' in sec_list:
            number = number + "_" + sec_list['le']
        elif sec_list:
            number=number+"_"+sec_list[list(sec_list.keys())[0]]


        department[short]["classes"][number] = {}
        department[short]["classes"][number]["course_number"] = number

        tname = str(cls.find("td", {"class": "crsheader"})).split(">")[1:]

        # sometimes there are rows before a class with special instructions
        if not "Units" in tname[0]:
            continue

        # number of units
        unit = tname[0].split("(")[1].split(")")[0].strip().replace("\t", "").replace("\n", "").replace("Units",
                                                                                                        "").strip()
        department[short]["classes"][number]["units"] = unit

        first = tname[0].split("(")[0].replace("\t", "").replace("\n", "").strip()

        if len(tname) > 2:
            second = tname[1].split("<")[0].replace("\n", "").replace("\t", "").strip()
            c_name = first + " " + second
        else:
            c_name = first

        # course name
        department[short]["classes"][number]["course_name"] = c_name
        department[short]["classes"][number]["sessions"] = {}

        # number of sessions; session: lecture, final, discussion, etc
        num_session = len(cls.find_all("span", {"id": "insTyp"}))

        # variables to iterate
        s = 0
        ll = 0
        w = 0

        # find index of sessions
        ses_idx = []
        sec_list = []
        for ii in range(cdl):
            c_detail = c_data[ii]
            spec = BeautifulSoup(str(c_detail), "html.parser")
            if spec.find("span", {"id": "insTyp"}) is not None:
                ses_idx.append(ii)
                section = str(BeautifulSoup(str(c_data[ii + 1]), "html.parser").string).strip().lower()
                if section is not None:
                    sec_list.append(section)

        for s in range(len(ses_idx)):

            # index of session
            w = ses_idx[s]

            # session type: le, fi, di, etc
            unparsed_type = BeautifulSoup(str(c_data[w]), "html.parser")
            typ = str(unparsed_type.find("span", {"id": "insTyp"}).string).lower().strip()

            s += 1

            if typ == 'fi':
                department[short]["classes"][number]["sessions"][typ] = {}
                date = str(BeautifulSoup(str(c_data[w + 1]), "html.parser").string).strip()
                department[short]["classes"][number]["sessions"][typ]["date"] = date
            else:
                # session id, i.e. 935299
                ses_id = str(BeautifulSoup(str(c_data[w - 1]), "html.parser").string).strip()

                # section such as A00, B00
                ll = w + 1
                section = str(BeautifulSoup(str(c_data[ll]), "html.parser").string).strip().lower()

                t_backup = typ
                typ = typ + "_" + section
                department[short]["classes"][number]["sessions"][typ] = {}
                department[short]["classes"][number]["sessions"][typ]["type"] = t_backup
                department[short]["classes"][number]["sessions"][typ]["session_id"] = ses_id

                department[short]["classes"][number]["sessions"][typ]["section"] = section

                ll += 1
                nxt = str(BeautifulSoup(str(c_data[ll]), "html.parser").string).strip()
                days = re.findall('[A-Z][^A-Z]*', nxt)

                if nxt == "TBA":
                    department[short]["classes"][number]["sessions"][typ]["days"] = nxt
                    department[short]["classes"][number]["sessions"][typ]["time"] = nxt
                    department[short]["classes"][number]["sessions"][typ]["building"] = nxt
                    department[short]["classes"][number]["sessions"][typ]["room_number"] = nxt
                elif not set(days) <= set(days_abb):
                    continue
                else:
                    department[short]["classes"][number]["sessions"][typ]["days"] = days
                    ll += 1
                    time = str(BeautifulSoup(str(c_data[ll]), "html.parser").string).strip()

                    time = parse(time)

                    time_obj={'start':{'hour':time[0] ,'minute':time[1]},'end':{'hour':time[2] ,'minute':time[3]}}

                    department[short]["classes"][number]["sessions"][typ]["time"] = time_obj
                    ll += 1
                    building = str(BeautifulSoup(str(c_data[ll]), "html.parser").string).strip().lower()
                    department[short]["classes"][number]["sessions"][typ]["building"] = building
                    ll += 1
                    room = str(BeautifulSoup(str(c_data[ll]), "html.parser").string).strip()

                    department[short]["classes"][number]["sessions"][typ]["room_number"] = room
                    room_id = building + "_" + room
                    department[short]["classes"][number]["sessions"][typ]["location"] = room_id

                    if room_id in room_freq:
                        room_freq[room_id] += 1
                    else:
                        room_freq[room_id] = 1

                ll += 1
                prof = str(BeautifulSoup(str(c_data[ll]).replace("<br/>", ""), "html.parser").string).replace("\n","").strip()
                department[short]["classes"][number]["sessions"][typ]["professor"] = prof

            s += 1

        num_classes += 1

    #print num_classes
    total_classes += num_classes

print("\nTotal number of classes: "+ str(total_classes))
print("\nTime to parse data: "+str(round(tm.time()-start_t,1))+" seconds")
UCSD['data'] = department
UCSD['department'] = d_list


#Write jsons
class_json_path=ospjoin(out_folder,str(year)+'_'+quarter+'_UCSD_classes.json')
freq_json_path=ospjoin(out_folder,str(year)+'_'+quarter+'_room_freq.json')
with open(class_json_path, 'w') as outfile:
    json.dump(UCSD, outfile)
print("\n\nWrote Quarter Json to " + class_json_path)

with open(freq_json_path, 'w') as outfile:
    json.dump(room_freq, outfile)
print("\nWrote Room Frequency Json to " + freq_json_path +"\n")
