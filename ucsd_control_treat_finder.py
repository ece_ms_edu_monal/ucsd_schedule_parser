import os
from os.path import expanduser
import json
import argparse

home = expanduser("./")


############################################
# Find all courses across two terms taught by the same professor, i.e. math20a taught in both fall and spring by prof. eggers
# option to select a room and find any course and prof matches in another term
############################################



# instantiate parser and parse args
parser = argparse.ArgumentParser()
parser.add_argument('--term1_json', dest='term1_json', required=True)
parser.add_argument('--term2_json', dest='term2_json', required=True)
parser.add_argument('--same_term', dest='same_term', required=False, default="False")
parser.add_argument('--room_to_match', default=0)
parser.add_argument('--output_location', default=home)

args = parser.parse_args()
term1_json = args.term1_json
term2_json = args.term2_json
out_folder = args.output_location
room_to_match = args.room_to_match
same_term = args.same_term

if not out_folder.endswith('/'):
    out_folder=out_folder+'/'

if not os.path.isfile(term1_json):
    print('file doesnt exist: ', term1_json)
    exit()
if not os.path.isfile(term2_json):
    print('file doesnt exist: ', term2_json)
    exit()

# load jsons
with open(term1_json) as data_file:
    term1_json_data = json.load(data_file)

with open(term2_json) as data_file:
    term2_json_data = json.load(data_file)
term1_short=''
term2_short=''
if '/' in term1_json:
    term1_short=term1_json.split('/')[-1][:-5]
else:
    term1_short=term1_json[:-5]
if '/' in term2_json:
    term2_short=term2_json.split('/')[-1][:-5]
else:
    term2_short=term2_json[:-5]

num_match=0
match_json={'total_course_matches':0,'departments':{}}
same_json={'courses':{}}
# find matches
if not room_to_match:
    # Iterate through departments
    for dpt, data in term1_json_data["data"].items():

        if dpt in term2_json_data['department']:
            term2_dpt_courses=term2_json_data['data'][dpt]['classes']
            term2_course_keys={'_'.join(course_check.split('_')[0:2]):course_check for course_check in term2_dpt_courses.keys()}
            dpt_has_match=0
            dpt_match_json={}
            # Iterate through classes in department
            for c_name, c_data in data["classes"].items():
                c_name_check=c_name
                c_name_check='_'.join(c_name.split('_')[0:2])
                sectionid_check=''
                if len(c_name.split('_'))>2:
                    sectionid_check=c_name.split('_')[2]

                # check for same course while ignoring the _a00 or _b00 section id of course
                if c_name_check in term2_course_keys.keys():
                    #same term double course check
                    if same_term and not (c_name.endswith('b00') or c_name.endswith('c00') or c_name.endswith('d00')):
                        continue
                    if same_term:

                        for ses_same_name, session_data in term2_dpt_courses[c_name]['sessions'].items():
                            sn_same = ses_same_name.split('_')[0]
                            if sn_same == 'le' or sn_same == 'se':
                                sn_same_key=ses_same_name
                                same_json['courses'][c_name_check]=term2_dpt_courses[c_name]['sessions'][sn_same_key]
                                print(c_name_check, ': ', c_data['course_name'])
                                num_match+=1
                                #print('\n',term2_dpt_courses[c_name]['sessions'][sn_same_key], '\n\n')

                    if not same_term and "sessions" in c_data:

                        for ses_name, session_data in c_data["sessions"].items():
                            sn = ses_name.split('_')[0]
                            if sn=='le' or sn =='se':
                                room=''
                                if "location" in session_data:
                                    room = session_data["location"]
                                sn = ses_name.split('_')[0]
                                if 'professor' in session_data:
                                    term_prof=session_data['professor']

                                    if "sessions" in term2_dpt_courses[term2_course_keys[c_name_check]]:

                                        other_term_sessions=term2_dpt_courses[term2_course_keys[c_name_check]]['sessions']

                                        other_term_prof=[]
                                        ots_match=''
                                        for ots_key in other_term_sessions.keys():
                                            if 'professor' in other_term_sessions[ots_key]:
                                                other_term_prof.append(other_term_sessions[ots_key]['professor'])

                                                if term_prof in other_term_sessions[ots_key]['professor']:
                                                    ots_match=ots_key


                                        if term_prof in other_term_prof:

                                            dpt_has_match=1
                                            t2_full=other_term_sessions[ots_match]
                                            t1_full=session_data
                                            num_match += 1
                                            dpt_match_json[c_name_check]={term1_short:t1_full,term2_short:t2_full}
                                            print(c_name_check, ': ', c_data['course_name'])
                                            print(ses_name ,'\n', t1_full, '\n', ots_match,'\n', t2_full)
                                            print('\n\n')

                                            #if 'location' in t1_full and 'location' in t2_full and t2_full['location']==t1_full['location']:




            if dpt_has_match:
                match_json['departments'][dpt]=dpt_match_json

if same_term:
    same_json['total_course_matches']=num_match
    out_name = out_folder + term1_short + '_' + term2_short + '_same_term.json'
    with open(out_name, 'w') as outfile:
        json.dump(same_json, outfile)
    print("\nWrote Same Term Match Json to " + out_name + "\n")

else:
    match_json['total_course_matches']=num_match
    out_name=out_folder+term1_short+'_'+term2_short+'_match.json'
    with open(out_name, 'w') as outfile:
        json.dump(match_json, outfile)
    print("\nWrote Match Json to " + out_name +"\n")
